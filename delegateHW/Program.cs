﻿// See https://aka.ms/new-console-template for more information

using System.Collections;

Console.WriteLine("Hello, World!");
var l = new List<Elem> { new Elem(23),new Elem(3), new Elem(76), new Elem(11), new Elem(17) };
var max = l.GetMax<Elem>(Elem.convFunc);
Console.WriteLine($"the max value in a list is {max.GetWeight()}"); ;

void YAF(object sender, EventArgs args)
{
    Console.WriteLine((args as FileArgs)?.FileName);
}

DirectoryScanner ds = new DirectoryScanner();
ds.OnFile += YAF;
ds.Scan(Path.GetDirectoryName((typeof(DirectoryScanner).Assembly.Location)));
ds.OnFile -= YAF;

public class Elem
{
    protected float weight;
    public Elem(float weight)
    {
        this.weight = weight;
    }
    public float GetWeight() => weight;
    public static float convFunc(Elem par) => par.GetWeight();
}

public static class GetMaxExtension
{
    public static T GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
    {
        var en = e.GetEnumerator();
        if (!en.MoveNext()) return null;
        var res = (T)en.Current;
        while (en.MoveNext())
        {
            if (getParameter((T)en.Current) > getParameter(res))
            {
                res = (T)en.Current;
            }
        }
        return res;
    }
}

public class FileArgs : EventArgs
{
    public string FileName { get; }
    public FileArgs(string fileName) => FileName = fileName;
}
public class DirectoryScanner
{
    public event EventHandler<FileArgs> OnFile;
    public void Scan(string directory)
    {
        foreach (var file in Directory.EnumerateFiles(directory)) FireFileFound(file);
    }
    void FireFileFound(string file) => OnFile?.Invoke(this, new FileArgs(file));
}



